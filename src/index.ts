import express, {Request, Response} from 'express'
import router from './router'

const server = express()
server.use(express.json())
server.use('/products', router)

server.get('/', (req: Request, res: Response) => {
    res.send("Versio 2")
})

const { PORT } = process.env
server.listen(PORT, () => {
    console.log('Products API listening to port', PORT)
})
